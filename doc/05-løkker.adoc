= Løkker og grener

Start et nytt prosjekt, og skriv følgende kode i `draw()`-funksjonen:

[source,javascript]
.game.js
--------
function draw(ctx) {
  var x = 0;
  var y = 0;
  var lengde = 100;

  ctx.fillStyle = "green";
  ctx.fillRect(x, y, lengde, lengde);
}
--------

Av det du har lært til nå burde det ikke overraske deg at dette tegner en firkant i øvre venstre hjørne av tegneområdet vårt. Firkanten er grønn. Til forskjell fra tidligere så har vi erstattet alle argumentene til `fillRect`-funksjonen med variabler. Det er mye lettere å skjønne hva funksjonen gjør når det er skrevet slik, er det ikke?

Sist lærte vi å flytte rundt på tegningen vi har laget ved å endre verdien av variablene som bestemte hvor tegningen skulle starte. Nå skal vi se på å bruke variablene til å lage flere utgaver av tegningen vår. For å gjøre det enkelt holder vi oss til firkanten over heller enn raketten i denne omgang.

== while()

Den enkleste løkken kalles en `while()`-løkke. Den ser sånn ut:

[source,javascript]
.game.js
--------
function draw(ctx) {
  var x = 0;
  var y = 0;
  var lengde = 100;

  while (x < 400) {
    ctx.fillStyle = "green";
    ctx.fillRect(x, y, lengde, lengde);
    x = x + lengde;
  }
}
--------

Det den gjør er at _så lenge_ uttrykket inne i parentesene er sant, så vil den kjøre koden som er inne i krøllparentesene under den. I tilfelle over kan vi lese det slik:

_Så lenge x er mindre enn 400_ så skal koden inne i løkken kjøres.

Koden inne i løkken ser kjent ut. De to første linjene vet vi hva gjør, men hva gjør den tredje?

Den _endrer_ verdien av variabelen `x`. I dette tilfellet setter den at fra nå av skal verdien av `x` være lik den gamle verdien av `x` pluss verdien av `lengde`. Det gjør at når løkken kjører neste gang så brukes den nye verdien av `x` til å bestemme hvor firkanten skal plasseres, og firkanten havner nå lengre mot høyre på tegningen. Dette gjentar seg helt til verdien av `x` er 400 eller større, da avsluttes løkken og programmet går videre.

[NOTE]
.ADVARSEL
======
Dersom vi glemmer å endre verdien av `x`, så vil aldri uttrykket som `while()`-løkken sjekker noensinne bli feil. Da har vi det som kalles en uendelig løkke, og det er som regel en litt kjipt feil!
======

Men koden over gir ikke et veldig spennende resultat. Vi tegner riktignok flere firkanter etter hverandre, men det ser jo ut som bare én lang firkant. Det kan vi gjøre noe med.

Legg til en ny variabel som heter `mellomrom`, som vi kan gi verdien 10 (eller noe annet hvis du vil.) Så kan vi endre siste linje inne i løkken slik:

[source, javascript]
x = x + lengde + mellomrom;

Hva skjer da?


== Løkker inni løkker

Koden inne i en løkke kan være hva som helst, også en annen løkke. La oss prøve det:

[source,javascript]
.game.js
--------
function draw(ctx) {
  var x = 0;
  var y = 0;
  var lengde = 100;
  var mellomrom = 10;

  while (y < 400) {
    while (x < 400) {
      ctx.fillStyle = "green";
      ctx.fillRect(x, y, lengde, lengde);
      x = x + lengde + mellomrom;
    }

    y = y + lengde + mellomrom;
    x = 0;
  }
}
--------

Her har vi lagt en ny løkke utenpå den vi hadde fra før. Det vil si at for hver gang den nye løkken kjører sin kode, så kjører den gamle løkken sin kode så mange ganger som den skal. Det høres litt komplisert ut? Så lenge vi tenker på løkkene hver for seg så skal det nok gå bra.

Den første løkken tegnet jo en rad av firkanter. Så denne nye løkken tegner da flere rader av firkanter. For hver rad så endrer vi verdien av `y`, slik at radene kommer under hverandre.

Legg merke til at vi setter verdien av `x` tilbake til 0 for hver gang vi har tegnet en rad, ellers ville det jo ikke bli noen flere rader siden x allerede er større eller lik 400!


== Avgjørelser - `if`

Nå kan vi å repetere kode, slik at vi får datamaskinen til å tegne mange firkanter for oss. Et annet viktig element av programmering er å kunne ta avgjørelser. Til det bruker vi en `if`-setning. Den ser slik ut:

[source,javascript]
.if.js
--------
if (uttrykk) {
  // kode
}
--------

På samme måte som `while()`-løkken så vil koden inne i `if`-setningen kjøre dersom uttrykket er sant. F.eks:

[source,javascript]
.if.js
--------
if (y < 200) {
  ctx.fillStyle = "red";
}
--------

Her vil vi sette verdien av variabelen `ctx.fillStyle` til "red" dersom verdien av `x` er mindre enn 200 når vi kommer til uttrykket. Dersom verdien av `x` er 200 eller større så hopper vi bare over koden inni og variabelen forblir uendret.

Dermed kan vi ta avgjørelser basert på verdien av en variabel.

La oss kombinere dette med firkantene våre over, det skal se omtrent slik ut:

[source,javascript]
.game.js
--------
function draw(ctx) {
  var x = 0;
  var y = 0;
  var lengde = 100;
  var mellomrom = 10;

  while (y < 400) {
    while (x < 400) {
      ctx.fillStyle = "green";

      if (x < 200) {
        ctx.fillStyle = "red";
      }

      ctx.fillRect(x, y, lengde, lengde);
      x = x + lengde + mellomrom;
    }

    y = y + lengde + mellomrom;
    x = 0;
  }
}
--------

Nå skal firkantene få forskjellig farge, alt etter hvor langt til høyre de befinner seg på tegningen vår. Men koden er ikke helt optimal. For hver gang vi kjører den innerste løkken så setter vi verdien av `ctx.fillStyle` opptil to ganger (når `x < 200`.) Det kan vi gjøre noe med.

`if`-setningen har ett ledd til som vi kan ta med, `else`. Det ser slik ut:

[source,javascript]
.if-else.js
------------
if (uttrykk) {
  // kode som kjøres hvis uttrykk er sant
}
else {
  // kode som kjøres hvis uttrykk ikke er sant
}
------------

Dermed kan vi endre den innerste løkken slik at den blir seende sånn ut:

[source,javascript]
.game.js
--------
while (x < 400) {
  if (x < 200) {
    ctx.fillStyle = "red";
  }
  else {
    ctx.fillStyle = "green";
  }

  ctx.fillRect(x, y, lengde, lengde);
  x = x + lengde + mellomrom;
}
--------

Da blir variabelen satt kun én gang hver gang løkken kjøres, men til forskjellig verdi avhengig av verdien av `x`.

Hele koden ser nå omtrent slik ut:

[source,javascript]
.game.js
--------
function draw(ctx) {
  var x = 0;
  var y = 0;
  var lengde = 100;
  var mellomrom = 10;

  while (y < 400) {
    while (x < 400) {
      if (x < 200) {
        ctx.fillStyle = "red";
      }
      else {
        ctx.fillStyle = "green";
      }

      ctx.fillRect(x, y, lengde, lengde);
      x = x + lengde + mellomrom;
    }

    y = y + lengde + mellomrom;
    x = 0;
  }
}
--------

Dette begynner å ligne på et ordentlig program! Vi bruker løkker til å kjøre kode flere ganger, og vi bruker `if`/`else`-setninger til å velge verdien til variable avhengig av verdien på andre variable! Klarer du å henge med å skjønne alt dette er du på god vei til å bli en ordentlig programmerer! *Godt jobba!*

== Oppgaver

1. Kan du endre programmet over slik at det blir forskjellig farge på de forskjellige radene også?
2. Kan du gjør slik at lengden på sidene på firkantene blir mindre eller større for hver rad?
3. Det samme som over med for hver kolonne?
4. Kan du endre koden i programmet med raketten så du får flere raketter ved siden av hverandre?
