function draw(ctx)
{
  var x = 0;
  var y = 0;
  var lengde = 100;
  var mellomrom = 10;

  while (y < 400)
  {
    while (x < 400)
    {
      if (x < 200)
      {
        ctx.fillStyle = "red";
      }
      else
      {
        ctx.fillStyle = "green";
      }

      ctx.fillRect(x, y, lengde, lengde);
      x = x + lengde + mellomrom;
    }

    y = y + lengde + mellomrom;
    x = 0;
  }
}

window.addEventListener('load', function() {
  console.log("Starting up!");

  var canvas = document.getElementById('game');
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');
    draw(ctx);
  }
});
